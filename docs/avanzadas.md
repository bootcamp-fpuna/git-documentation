## Ramas
### Crear ramas
Supongamos que queremos crear una rama nueva denominada "testing". Para ello, usamos el comando **git branch**:

```console
$ git branch testing
```
Esto creará un nuevo apuntador apuntando al mismo commit donde nos encontremos actualmente.

![Dos ramas](img/two-branches.png)

Y, ¿cómo sabe Git en qué rama estás en este momento? Pues…​, mediante un apuntador especial denominado HEAD.En Git, es simplemente el apuntador a la rama local en la que tú estés en ese momento, en este caso la rama master; pues el comando **git branch** solamente crea una nueva rama, pero no salta a dicha rama.

### Cambiar de Rama
Para saltar de una rama a otra, tienes que utilizar el comando git checkout.
```console
git checkout testing
```

Cuando hacemos checkout en ramas sin relación se realizan 2 acciones: 1) Mueve el apuntador HEAD a la nueva rama, y 2) revierte los archivos de tu directorio de trabajo; dejándolos tal y como estaban en la última instantánea del ultimo commit de la rama.

Si utilizamos el parametro -b con el commando checkout se crea en forma automatica una nueva rama y se coloca el head sobre la nueva rama (seria como una simplificacion de los 2 comando anteriores)

### Gestion de ramas

El comando git branch tiene más funciones que las de crear y borrar ramas. Si  lanzas sin parámetros, obtienes una lista de las ramas presentes en tu proyecto:
```console
$ git branch
  iss53
* master
  testing
```

Fijate en el carácter * delante de la rama master: nos indica la rama activa en este momento (la rama a la que apunta HEAD). Si hacemos una confirmación de cambios (commit), esa será la rama que avance. Para ver la última confirmación de cambios en cada rama, puedes usar el comando git branch -v:

```console
$ git branch -v
  iss53   93b412c fix javascript issue
* master  7a98805 Merge branch 'iss53'
  testing 782fd34 add scott to the author list in the readmes
```

Otra opción útil para averiguar el estado de las ramas, es filtrarlas y mostrar solo aquellas que han sido fusionadas (o que no lo han sido) con la rama actualmente activa. Para ello, Git dispone de las opciones --merged y --no-merged. Si deseas ver las ramas que han sido fusionadas con la rama activa, puedes lanzar el comando git branch --merged:

```console
$ git branch --merged
  iss53
* master
```

Aparece la rama iss53 porque ya ha sido fusionada. Las ramas que no llevan por delante el carácter * pueden ser eliminadas sin problemas, porque todo su contenido ya ha sido incorporado a otras ramas.

Para mostrar todas las ramas que contienen trabajos sin fusionar, puedes utilizar el comando git branch --no-merged:

```console
$ git branch --no-merged
  testing
```

### Ramas Remotas

Las ramas remotas son referencias al estado de las ramas en tus repositorios remotos. Son ramas locales que no puedes mover; se mueven automáticamente cuando estableces comunicaciones en la red. Las ramas remotas funcionan como marcadores, para recordarte en qué estado se encontraban tus repositorios remotos la última vez que conectaste con ellos.

Suelen referenciarse como (remoto)/(rama). Por ejemplo, si quieres saber cómo estaba la rama master en el remoto origin, puedes revisar la rama origin/master. O si estás trabajando en un problema con un compañero y este envía (push) una rama iss53, tú tendrás tu propia rama de trabajo local iss53; pero la rama en el servidor apuntará a la última confirmación (commit) en la rama origin/iss53.

Esto puede ser un tanto confuso, pero intentemos aclararlo con un ejemplo. Supongamos que tienes un servidor Git en tu red, en git.ourcompany.com. Si haces un clon desde ahí, Git automáticamente lo denominará origin, traerá (pull) sus datos, creará un apuntador hacia donde esté en ese momento su rama master y denominará la copia local origin/master. Git te proporcionará también tu propia rama master, apuntando al mismo lugar que la rama master de origin; de manera que tengas donde trabajar.

![Ramas remotas](img/remote-branches-1.png)

Si haces algún trabajo en tu rama master local, y al mismo tiempo, alguien más lleva (push) su trabajo al servidor git.ourcompany.com, actualizando la rama master de allí, te encontrarás con que ambos registros avanzan de forma diferente. Además, mientras no tengas contacto con el servidor, tu apuntador a tu rama origin/master no se moverá.

![Ramas remotas](img/remote-branches-2.png)

Para sincronizarte, puedes utilizar el comando git fetch origin. Este comando localiza en qué servidor está el origen (en este caso git.ourcompany.com), recupera cualquier dato presente allí que tú no tengas, y actualiza tu base de datos local, moviendo tu rama origin/master para que apunte a la posición más reciente.

![Ramas remotas](img/remote-branches-3.png)

#### Publicar ramas remotas

Cuando quieres compartir una rama con el resto del mundo, debes llevarla (push) a un remoto donde tengas permisos de escritura. Tus ramas locales no se sincronizan automáticamente con los remotos en los que escribes, sino que tienes que enviar (push) expresamente las ramas que desees compartir. De esta forma, puedes usar ramas privadas para el trabajo que no deseas compartir, llevando a un remoto tan solo aquellas partes que deseas aportar a los demás.

Si tienes una rama llamada serverfix, con la que vas a trabajar en colaboración; puedes llevarla al remoto de la misma forma que llevaste tu primera rama. Con el comando git push (remoto) (rama):

```console
$ git push origin serverfix
Counting objects: 24, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (15/15), done.
Writing objects: 100% (24/24), 1.91 KiB | 0 bytes/s, done.
Total 24 (delta 2), reused 0 (delta 0)
To https://github.com/schacon/simplegit
 * [new branch]      serverfix -> serverfix
```

### Mezclar ramas
Para mezclar el contenido de ramas debemos posicionarnos en la rama destino con el comando checkout y luego ejecutar el comando merge.
```console
$ git checkout master
Switched to branch 'master'
$ git merge devel
Merge made by the 'recursive' strategy.
index.html |    1 +
1 file changed, 1 insertion(+)
```
![Conflicto](img/meme-merge.jpeg)


### Principales Conflictos que pueden Surgir en las mezclas
En algunas ocasiones, los procesos de mezcla no suelen ser fluidos. Si hay modificaciones dispares en una misma porción de un mismo archivo en las dos ramas distintas que pretendes mezclar, Git no será capaz de fusionarlas directamente. 
![Conflicto](img/merge-1.gif)

Por ejemplo, si en tu trabajo del problema #53 has modificado una misma porción que también ha sido modificada en el problema hotfix, verás un conflicto como este:
```console
$ git merge iss53
Auto-merging index.html
CONFLICT (content): Merge conflict in index.html
Automatic merge failed; fix conflicts and then commit the result.
```

Git no crea automáticamente una nueva mezcla confirmada (merge commit), sino que hace una pausa en el proceso, esperando a que tú resuelvas el conflicto. Para ver qué archivos permanecen sin fusionar en un determinado momento conflictivo de una fusión, puedes usar el comando git status:

```console
$ git status
On branch master
You have unmerged paths.
  (fix conflicts and run "git commit")

Unmerged paths:
  (use "git add <file>..." to mark resolution)

    both modified:      index.html

no changes added to commit (use "git add" and/or "git commit -a")
```

Todo aquello que sea conflictivo y no se haya podido resolver, se marca como "sin fusionar" (unmerged). Git añade a los archivos conflictivos unos marcadores especiales de resolución de conflictos que te guiarán cuando abras manualmente los archivos implicados y los edites para corregirlos. El archivo conflictivo contendrá algo como:

```console
<<<<<<< HEAD:index.html
<div id="footer">contact : email.support@github.com</div>
=======
<div id="footer">
 please contact us at support@github.com
</div>
>>>>>>> iss53:index.html
```

Donde nos dice que la versión en HEAD (la rama master, la que habías activado antes de lanzar el comando de fusión) contiene lo indicado en la parte superior del bloque (todo lo que está encima de =======) y que la versión en iss53 contiene el resto, lo indicado en la parte inferior del bloque. Para resolver el conflicto, has de elegir manualmente el contenido de uno o de otro lado. Por ejemplo, puedes optar por cambiar el bloque, dejándolo así:

```console
<div id="footer">
please contact us at email.support@github.com
</div>
```

Tras resolver todos los bloques conflictivos, has de lanzar comandos git add para marcar cada archivo modificado. Marcar archivos como preparados (staged) indica a Git que sus conflictos han sido resueltos.

Si todo ha ido correctamente, y ves que todos los archivos conflictivos están marcados como preparados, puedes lanzar el comando git commit para terminar de confirmar la fusión. 
