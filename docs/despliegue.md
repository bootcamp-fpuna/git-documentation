# Despliegue en la nube 
## Trabajemos con los repositorios
Antes de comenzar debemos hablar de algunos conceptos en la nube, principalmente orientados a los tipos de servicios.

Los servicios en la nube van desde el almacenamiento de datos hasta los programas funcionales, como los software de contabilidad, las herramientas de servicio al cliente y el alojamiento de escritorio remoto. 

![Tipos de servicios en la nube](img/cloud.png)


Estos servicios se clasifican básicamente en tres grupos:

* **Infraestructura como servicio (IaaS)**: proporciona una infraestructura informática que se aprovisiona y administra a través de internet. Un proveedor de IaaS gestiona la parte física de la infraestructura (como servidores o espacio de almacenamiento de datos) en un centro de datos, pero permite a los clientes personalizar completamente esos recursos virtualizados para satisfacer sus necesidades específicas. 
* **Plataforma como servicio (PaaS)**: A través de este servicio en la nube, los desarrolladores de software tienen acceso a herramientas basadas en la nube como API, software de puerta de enlace o portales web. Servicios como Lightning de Salesforce, Google App Engine y Elastic Beanstalk de AWS son soluciones populares. Bajo estas soluciones, el Proveedor de la solución PaaS administra la infraestructura y también la capa del sistema operativo. 
* **Software como servicio (SaaS)**:Este tipo de servicio brinda a los usuarios acceso a software a través de internet. Las aplicaciones SaaS, accesibles a través de computadoras y dispositivos móviles con acceso a internet, permiten a los trabajadores colaborar en proyectos, descargar archivos importantes y trabajar directamente en programas informáticos especializados. 

### Firebase
Firebase es una popular solución de servicios en la nube equipada con una infraestructura sólida y que brinda múltiples servicios. 

Se utiliza principalmente para casos de uso de alojamiento de aplicaciones y sitios web y los usuarios lo prefieren debido a su facilidad de uso. 

Los usuarios de la plataforma pueden administrar sus aplicaciones a través de una interfaz de usuario basada en web o CLI (Command Line Interface, o interfaz de línea de comandos).


### Levantar la plaza en un servicio en la nube
#### Prerequisito
Instalar la herramienta [Node](https://nodejs.org/en/download/) LTS para Windows.

Acceder y crear un usuario en [Firebase](https://firebase.google.com/?hl=es)

<div style="position: relative; padding-bottom: 62.5%; height: 0;"><iframe src="https://www.loom.com/embed/a9068c375c9f4dc2826f1866eb9180a0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>