# Git
Es muy importante entender esta sección, porque si comprendemos bien lo que es Git y los fundamentos de cómo funciona, probablemente  sea mucho más fácil usar Git de manera eficaz.

Git es una sistema de gestión de versiones distribuido que nos permite administrar los cambios de nuestros archivos.

## Instantáneas, no diferencias
La principal diferencia entre Git y cualquier otro VCS (Subversion y compañía incluidos) es cómo Git modela sus datos. Conceptualmente, la mayoría de los demás sistemas almacenan la información como una lista de cambios en los archivos. Modelan la información que almacenan como un conjunto de archivos y las modificaciones hechas sobre cada uno de ellos a lo largo del tiempo.

En cambio, Git modela sus datos más como un conjunto de instantáneas de un mini sistema de archivos. Cada vez que confirmamos un cambio, o guardamos el estado de un proyecto en Git, él básicamente hace una foto del aspecto de todos tus archivos en ese momento, y guarda una referencia a esa instantánea. Para ser eficiente, si los archivos no se han modificado, Git no almacena el archivo de nuevo, sólo un enlace al archivo anterior idéntico que ya tiene almacenado.

![Comparación con SVN](img/git_diff_svn.png)

## Casi cualquier operación es local
La mayoría de las operaciones en Git sólo necesitan archivos y recursos locales para operar. Como tenemos toda la historia del proyecto ahí mismo, en tu disco local, la mayoría de las operaciones parecen prácticamente inmediatas.

## Git tiene integridad
Todo en Git es verificado mediante una suma de comprobación antes de ser almacenado, y es identificado a partir de ese momento mediante dicha suma (*checksum* en inglés). Esto significa que es imposible cambiar los contenidos de cualquier archivo o directorio sin que Git lo sepa.

## Git generalmente sólo añade información
Cuando realizas acciones en Git, casi todas ellas sólo añaden información a la base de datos de Git. Es muy difícil conseguir que el sistema haga algo que no se pueda deshacer, o que de algún modo borre información.

## Los tres estados
Git tiene tres estados principales en los que se pueden encontrar tus archivos: confirmado (*committed*), modificado (*modified*), y preparado (*staged*).

**Confirmado** significa que los datos están almacenados de manera segura en tu base de datos local. **Modificado** significa que has modificado el archivo pero todavía no lo has confirmado a tu base de datos. **Preparado** significa que has marcado un archivo modificado en su versión actual para que vaya en tu próxima confirmación.

Esto nos lleva a las tres secciones principales de un proyecto de Git: el directorio de Git (*Git directory*), el directorio de trabajo (*working directory*), y el área de preparación (*staging area*).

![Flujo básico de trabajo](img/git_work.png)

El directorio de Git (o repositorio) es donde Git almacena los metadatos y la base de datos de objetos para tu proyecto. Es la parte más importante de Git, y es lo que se copia cuando clonamos un repositorio desde otro ordenador.

El directorio de trabajo es una copia de una versión del proyecto. Estos archivos se sacan de la base de datos comprimida en el directorio de Git, y se colocan en disco para que los podamos usar o modificar.

El área de preparación es un sencillo archivo, generalmente contenido en nuestro directorio de Git, que almacena información acerca de lo que va a ir en tu próxima confirmación. A veces se denomina el índice, pero se está convirtiendo en estándar el referirse a ello como el área de preparación.

El flujo de trabajo básico en Git es algo así:

* Modificamos una serie de archivos en tu directorio de trabajo.
* Preparamos los archivos, añadiendo instantáneas de ellos a tu área de preparación.
* Confirmamos los cambios, lo que toma los archivos tal y como están en el área de preparación, y almacena esa instantánea de manera permanente en tu directorio de Git.

Si una versión concreta de un archivo está en el directorio de Git, se considera confirmada (*committed*). Si ha sufrido cambios desde que se obtuvo del repositorio, pero ha sido añadida al área de preparación, está preparada (*staged*). Y si ha sufrido cambios desde que se obtuvo del repositorio, pero no se ha preparado, está modificada (*modified*).



