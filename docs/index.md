# Bienvenidos al Módulo de Git

....

## Control de versiones

Se llama control de versiones a los métodos y herramientes disponibles para controlar todo lo referente a los cambios en el tiempo de un archivo.

Nos parece familiar esta imagen?

![Control de versiones](img/cv_manual.png)

En documentos sencillos o pequeños el control de versiones no es esencial. Pero cuando escribimos un libro o un programa con muchas lineas de código esto se puede volver complicado.

Actualmente la mayoría de los editores de texto (Word, Write, etc) o los editores en linea de documentos (Google Docs, Overleaf, etc) tienen herramientas incluidas para realizar seguimiento de cambios.

## Sistemas de control de versiones (VCS)

Con el tiempo han sido creadas herramientas especificas de control de versiones. Estas herramientas de dividen en 3 tipos:

### Locales
Este metodo es utilizado por mucha gente. Se basa en copiar los archivos a otro directorio y se indica la version por alguna cadena unica (generalmente se usa le fecha). Esta técnica es muy común porque es muy simple, pero también es muy facil equivocarse. Es fácil olvidar en qué directorio trabajamos, y sobrescribir archivos que no queremos.

![Gestión de versiones local](img/git_local.png)

Para resolver este problema, los programadores desarrollaron hace tiempo VCSs locales que manejan una base de datos que gestiona los cambios realizados sobre los archivos.


### Centralizadas
El siguiente problema que tienen las personas es que necesitan colaborar con desarrolladores en otros sistemas. Para contrarestar este problema, se desarrollaron los sistemas de control de versiones centralizados (*Centralized Version Control Systems* - CVCSs). Estos sistemas tienen un único servidor que contiene todos los archivos versionados, y varios clientes que descargan los archivos de ese lugar central. Durante muchos años, éste ha sido el estándar para el control de versiones 

![Gestión de versiones centralizada](img/git_centralizado.png)

Esta configuración ofrece muchas ventajas, especialmente frente a los entornos locales. Por ejemplo, todos los involucrados saben hasta cierto punto en qué está trabajando los otros miembros del proyecto, la identificación de las inclusiones, modificaciones y correcciones realizadas por cada miembro.

Sin embargo, esta configuración también tiene desventajas. La más obvia es el punto único de fallo que representa el servidor centralizado. Si ese servidor se cae durante una hora, entonces durante esa hora nadie puede colaborar o guardar cambios versionados de aquello en que están trabajando. Asi tambien, si el servidor centralizado tiene problemas de hardware, como disco, se pierde toda la información del versionamiento.

### Distribuidas (DVCS)

En un DVCS (como Git, Mercurial, Bazaar o Darcs), los clientes no sólo descargan la última instantánea de los archivos: replican completamente el repositorio. Así, si un servidor no esta disponible, y estos sistemas estaban colaborando a través de él, cualquiera de los repositorios de los clientes puede copiarse en el servidor para restaurarlo. Cada vez que se descarga una instantánea, en realidad se hace una copia de seguridad completa de todos los datos.

![Gestión de versiones distribuida](img/git_distribuido.png)

<blockquote class="twitter-tweet"><p lang="zxx" dir="ltr"><a href="https://t.co/4UQlClBSct">pic.twitter.com/4UQlClBSct</a></p>&mdash; Elon Musk (@elonmusk) <a href="https://twitter.com/elonmusk/status/1588945917217951746?ref_src=twsrc%5Etfw">November 5, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>