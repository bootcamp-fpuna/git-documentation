# Instalación de Git

Antes de empezar a utilizar Git, tienes que instalarlo en tu computadora.

### Instalación en Windows
Hay varias maneras de instalar Git en Windows. La forma más oficial está disponible para ser descargada en el sitio web de [Git](http://git-scm.com/download/win). Solo tienes que visitar  y la descarga empezará automáticamente. 

Puedes descargar la versión que incluye el instalador u otra portable que podrás ejecutar en cualquier ordenador. En este tutorial usaremos el instalador, que dependiendo de tu sistema operativo, podrá ser de 32 o de 64 bits.

Tras descargar el archivo, ejecútalo y sigue las instrucciones que se muestran a continuación:

1. Primero acepta las condiciones que se muestran en pantalla y haz clic en Next para continuar.

![Condiciones](img/git_install_1.png)

2. Luego selecciona el directorio de instalación y haz clic en Next para continuar.

![Ubicación](img/git_install_2.png)

3. Seleccionar los componentes que se instalarán. Es recomendable que selecciones los componentes que integrarán Git con el explorador de Windows para así evitar navegar excesivamente por los directorios usando la terminal.

![Componentes](img/git_install_3.png)

4. Seleccionar el directorio en el que se crearán los accesos directos del menú de inicio (Solo Windows 7, Windows 8 y Windows XP). Haz clic en Next para continuar.

![Directorio](img/git_install_4.png)

5. Ahora tendrás que seleccionar el editor que usará Git de forma predeterminada. Puedes seleccionar Vim, Nano, Notepad++ o VS Code.

![Editor](img/git_install_5.png)

6. Git ha decidido que su rama principal por defecto deje de ser *master* y pase a ser *main* por temas de inclusividad racial. Por ello y dependiendo de la versión de Git que instales, es posible que se te pregunte si prefieres usar la rama de Git por defecto, o si por el contrario quieres especificar otra. Escoge la opción «Let Git decide» y haz clic en Next para continuar:

![Ramas](img/git_install_6.png)

7. Luego tendrás que seleccionar las opciones de configuración del PATH de Windows. Utilizaremos la opción recomendada, «Git form the command line and also from 3rd-pary software», ya que la última opción podría sobrescribir ciertos comandos de Windows.

![Path](img/git_install_7.png)

8. Ahora tendrás que seleccionar el tipo de librería que usará Git para gestionar las conexiones SSL/TLS. Lo recomendable es que uses la opción por defecto, que es la librería OpenSSL. Haz clic en Next para continuar.

![SSL](img/git_install_8.png)

9. A continuación tendrás que seleccionar el trato que Git dará a los marcadores de final de línea, puesto que Windows y los sistemas Unix difieren en este aspecto. Selecciona la opción «Checkout Windows-style, commit Windows-style line endings», que es la opción por defecto, maximizando la compatibilidad entre sistemas Unix y Windows. Haz clic en Next para continuar.

![EOF](img/git_install_9.png)

10. A continuación tendrás que escoger el tipo de terminal que se utilizará como interfaz. Podrás escoger MinTTY, que es la opción por defecto y la recomendada, o la típica terminal de Windows. 

![Terminal](img/git_install_10.png)

11. Luego seleccionaremos el funcionamiento que tendrá el comando git pull. Escoge la opción por defecto.

![Pull](img/git_install_11.png)

12. Aplicación que se usará para gestionar las credenciales de Git. Puedes escoger entre Git Credential Manager Core o Git Credential Manager para Windows. Seleccionar la recomendada.

![Credenciales](img/git_install_12.png)

13. A continuación deja activada la opción que activa la caché de Git y activa también la compatibilidad con enlaces simbólicos. Luego haz clic en Next para continuar.

![Cache](img/git_install_13.png)

14. Una vez finalizada la instalación, haz clic en Next para continuar e iniciar Git Bash si así lo deseas.

![End](img/git_install_14.png)


### BONUS: Instalación en Linux
Si quieres instalar Git en Linux a través de un instalador binario, en general puedes hacerlo mediante la herramienta básica de administración de paquetes que trae tu distribución. Si estás en Fedora por ejemplo, puedes usar yum: 

```console
$ yum install git
```

Si estás en una distribución basada en Debian como Ubuntu, puedes usar apt-get:

```console
$ apt-get install git
```



## Antes de comenzar: CLI

La interfaz de línea de comandos o interfaz de línea de órdenes (en inglés, ***command-line interface***, CLI) es un tipo de interfaz de usuario de computadora que permite a los usuarios dar instrucciones a algún programa informático o al sistema operativo por medio de una línea de texto simple.

En el [material introductorio](https://danielferszt.github.io/bootcamp-fpuna-programacion-web/2_herramientas/1_Herramientas.html#opciones-de-l%C3%ADnea-de-comandos-cli-populares) hemos revisado los tipos de terminales de acuerdo al sistema operativo.

[Aqui](https://cheatography.com/jonathan992/cheat-sheets/gnu-linux-command-spanish/) tienen un resumen de los comandos mas utilizados.

## Configuración inicial
Es necesario hacer estos pasos solamente una vez en tu computadora, y se mantendrán entre actualizaciones. También puedes cambiarlas en cualquier momento volviendo a ejecutar los comandos correspondientes.

Git trae una herramienta llamada *git config*, que te permite obtener y establecer variables de configuración que controlan el aspecto y funcionamiento de Git. Estas variables pueden almacenarse en tres sitios distintos:

1. **Archivo /etc/gitconfig**: Contiene valores para todos los usuarios del sistema y todos sus repositorios. Si pasas la opción --system a git config, lee y escribe específicamente en este archivo.

2. **Archivo ~/.gitconfig o ~/.config/git/config**: Este archivo es específico de tu usuario. Puedes hacer que Git lea y escriba específicamente en este archivo pasando la opción --global.

3. **Archivo config en el directorio de Git (es decir, .git/config) del repositorio que estés utilizando actualmente**: Este archivo es específico del repositorio actual.

Cada nivel sobrescribe los valores del nivel anterior, por lo que los valores de .git/config tienen preferencia sobre los de /etc/gitconfig.

#### Identidad
Lo primero que deberás hacer cuando instales Git es establecer tu nombre de usuario y dirección de correo electrónico. Esto es importante porque los "commits" de Git usan esta información, y es introducida de manera inmutable en los commits que envías:

```console
$ git config --global user.name "Bootcamp FPUNA"
$ git config --global user.email bootcamp@pol.una.py
```

Sólo necesitas hacer esto una vez si especificas la opción --global, ya que Git siempre usará esta información para todo lo que hagas en ese sistema. Si quieres sobrescribir esta información con otro nombre o dirección de correo para proyectos específicos, puedes ejecutar el comando sin la opción --global cuando estés en ese proyecto.

#### Editor
Puedes elegir el editor de texto por defecto que se utilizará cuando Git necesite que introduzcas un mensaje. Si no indicas nada, Git usará el editor por defecto de tu sistema, que generalmente es Vim. Si quieres usar otro editor de texto como Emacs, puedes hacer lo siguiente:

```console
$ git config --global core.editor emacs
```

### Visualización de la configuración
Puedes usar el comando git config --list para mostrar todas las propiedades que Git ha configurado:
```console

$ git config --list
user.name=John Doe
user.email=johndoe@example.com
color.status=auto
color.branch=auto
color.interactive=auto
color.diff=auto
...
```

También puedes comprobar el valor que Git utilizará para una clave específica ejecutando git config <key>:

```console
$ git config user.name
Bootcamp FPUNA
```

### Acceso a la ayuda
Si alguna vez necesitas ayuda usando Git, existen tres formas de ver la página del manual (manpage) para cualquier comando de Git:

```console
$ git help <verb>
$ git <verb> --help
$ man git-<verb>
```

## Plataformas en la nube
[Gitlab](https://gitlab.com/) es una plataforma de creación de aplicaciones en la nube basada en el repositorio de código Git, que facilita el trabajo colaborativo. Existen otros herramientas como [Github](https://github.com/), [BitBucket](https://bitbucket.org/), entre otros.

Estas herramientas cumplen el papel principal de servidor centralizado, ademas de posee un conjunto de herramientas asociadas al los proyectos de software.

Gitlab tambien permite descargar el software e instalarlo como un servicio propio.

### Crear cuenta en GitLab
<div style="position: relative; padding-bottom: 62.5%; height: 0;"><iframe src="https://www.loom.com/embed/cbaee4b9c9e744678219ced7fe9695d2" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;"></iframe></div>